//=============================================================================
// Authors: Daniel Ramirez, Ramon Collazo
// Stu No : 801-12-6735, 801-12-1480
// emails : bojangles7856@gmail.com, rlcmartis@gmail.com

// Description:  Program to detect wrongly spelled words in a document.
// This version only reads the contents of the dictionary file and the 
// document file and prints.
//=============================================================================

#include <iostream>
#include <tr1/unordered_set>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>
#include <ctime>
using namespace std;


char lowerCase(char c) {
  if (c<'A' || c > 'Z') return c;
  return c + ('a'- 'A');
}

// Given a string, this function strips any non-letter chars from the end 
// of the string and converts to lower case
void strip_end_puntuation(string &st) {
	if ( (st.c_str()[st.length()-1] < 'a') ||  (st.c_str()[st.length()-1] > 'z') ) {
		st.erase(st.length()-1,1);
	}
	std::transform(st.begin(), st.end(), st.begin(), lowerCase);
}

void con_set(ifstream &dic, ifstream &book){
	int conta = 0;
	string word;
        string apos;
	set<string> correctas;

	// Reads the contents of the dictionary file and insert them in the set
	while ( dic.good() ) {
		dic >> word;
		correctas.insert(word);
	}

	// Reads the contents of the document file and output how many wrong
	// words are in the book
	while( !book.eof() ) {
		book >> word;
		strip_end_puntuation(word);
		//If the word has an apostrophe, verify without it
		if (word.size() > 2){ 
		    apos=word[word.size() -2];
		    //Holds the would be apostrophe
		    if(apos == "'")
			word.erase(word.length() -2, 2);
		}

		//If the word is not in the dictionarry, add one to the counter
		if (word.length() > 0 && 
		    correctas.find(word) == correctas.end()){
			cout << endl << word << endl;
			conta++;
		}
	}
	cout << conta << " incorrect words.\n";
}

void con_uset(ifstream &dic, ifstream &book){
	int conta = 0;  //Holds the counter of mispelled words
	string word;
	string apos;
	tr1::unordered_set<string> correctas2;

	// Reads the contents of the dictionary file and insert them in the set
        while ( dic.good() ) {
                dic >> word;
                correctas2.insert(word);
        }
        
        // Reads the contents of the document file and output how many wrong
        // words are in the book
        while ( !book.eof() ) {
        	book >> word;
                strip_end_puntuation(word);
		//If the word has an apostrophe, verify without it 
		 if (word.size() > 2){
			//Holds the would be apostrophe
                 	apos = word[word.size() -2];
			//Verifies if it really is an apostrophe
        	 	if(apos == "'")
                	        word.erase(word.length() -2, 2);
                }

    		//If the word is not in the dictionarry, add one to the counter
                if (word.length() > 0 &&
                    correctas2.find(word) == correctas2.end()){
                        conta++;
		}
        }
        cout << conta << " incorrect words.\n";
}

int main () {
  ifstream dic ("corncob_lowercase.txt");
  ifstream book ("book.txt");
  float time1, time2, mintime;
  // Just read the contents of the document file and outputs
  // words in lower case
  if (dic.is_open() && book.is_open()){
    clock_t t;

    //Calculates the time for the ordered set
    cout << "Using set:\n";
    t = clock();
    con_set(dic, book);
    t = clock() - t;

    time1 = static_cast<float>(t)/CLOCKS_PER_SEC;
    cout << time1 << " time elapsed.\n"; 
    //Assumes the first time is the fastest
    mintime = time1;

    //Closes and opens both istreams so we can access their data again
    dic.close();
    book.close();
    dic.open("corncob_lowercase.txt");
    book.open("book.txt");

    //Displays the unordered set
    cout << "Using unorderd set:\n";
    t = clock();
    con_uset(dic, book);
    t = clock() - t;

    time2 = static_cast<float>(t)/CLOCKS_PER_SEC;
    cout << time2 << " time elapsed.\n";
 
    //If the unordered set is faster, then the min is it's time  
    if(time2 < mintime)
	mintime = time2;

    //Depending which time is smaller, displays witch set is faster
    if (mintime == time2) 
	cout << endl << "The unordered set is faster.";
    else 
	cout << endl << "The ordered set is faster.";
    cout << endl;
    //Closes the files
    dic.close();
    book.close();
  }
  return 0;
}
